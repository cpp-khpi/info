# C Programming

## Requirements

### :heart: Register on the [gitlab.com](https://gitlab.com/)
- Use [Ukrainian transliteration](http://translit.kh.ua/?lat&passport) of your real *first* and *last* name.
- Be sure to specify **Username** as *lastname_firstname* in lowercase Latin.

### :yellow_heart: Read these instructions
- [Start using Git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).
- [Edit files through the command line](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html).

### :green_heart: Follow this [guide](https://gitlab.com/cpp-khpi/info/-/raw/master/doc/part01.pdf?inline=false).

### [Enjoy!](https://dou.ua/)

---

## Resources

### :computer: Development tools

- [GNU Compiler Collection](https://en.wikipedia.org/wiki/GNU_Compiler_Collection).
- [Clang](https://en.wikipedia.org/wiki/Clang) with [LLVM](https://en.wikipedia.org/wiki/LLVM) ([Ubuntu packages](https://apt.llvm.org/), [download page](http://releases.llvm.org/download.html)). Handy tools: [LeakSanitizer](https://clang.llvm.org/docs/LeakSanitizer.html), [scan-build](https://clang-analyzer.llvm.org/scan-build.html), [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html).
- [Cppcheck](http://cppcheck.sourceforge.net/).
- [Valgrind](http://valgrind.org/).
- [Git](https://git-scm.com/).
- [GitLab](https://gitlab.com/).

### :cloud: Online compilers and IDEs

- [JDoodle](https://www.jdoodle.com/). Easy and Quick way to run Programs Online.
- [GDB Online](https://www.onlinegdb.com/). Online compiler and debugger tool for C/C++ languages.
- [Coding Ground](https://www.tutorialspoint.com/codingground.htm). An online Lab for IT Professionals.

### :mag: References

- [Wiki. ANSI C](https://en.wikipedia.org/wiki/ANSI_C).
- [ISO/IEC 9899:1999 - Standard for Programming Language C (C99). The latest draft](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf).
- [ISO/IEC 9899:2011 - Standard for Programming Language C (C11). The latest draft](http://www.open-std.org/jtc1/sc22/WG14/www/docs/n1570.pdf).
- [ISO/IEC 9899:2018 - Standard for Programming Language C (С17/C18). The latest draft](https://web.archive.org/web/20181230041359if_/http://www.open-std.org/jtc1/sc22/wg14/www/abq/c17_updated_proposed_fdis.pdf).
- [ISO/IEC 9899:yyyy - Standard for Programming Language C (C2x). The latest draft](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n2479.pdf).
- [SEI CERT C Coding Standard](https://wiki.sei.cmu.edu/confluence/display/c/SEI+CERT+C+Coding+Standard).
- [C Reference](https://en.cppreference.com/w/c).
- [GNU Make Manual](https://www.gnu.org/software/make/manual/).
- Making Code More Secure with GCC: [Part 1](https://blogs.oracle.com/linux/making-code-more-secure-with-gcc-part-1), [Part 2](https://blogs.oracle.com/linux/making-code-more-secure-with-gcc-part-2).

---

## Notes

### :pushpin: Configure Git

```bash
# Set user name and email:
git config --global user.name "Name Surname"
git config --global user.email "mailbox@example.com"

# Set Nano as the default text editor:
git config --global core.editor "nano"

# Configure a global proxy if all access to all repos require this proxy:
git config --global http.proxy "172.17.10.2:3128"

# Unset a proxy
git config --global --unset http.proxy
```

### :bulb: Commit requirements

- Do not add binaries other than reports to avoid penalties.
- Source code at each commit should be buildable and working.
- Always check your code for [code style](https://www.kernel.org/doc/html/latest/process/coding-style.html) compliance.
- Commit **message** should be like: `git commit -m "topic: action the_rest_message_text"`

	- Do **not** use **Cyrillic**.
	- **topic** describes the area of changes, e.g. {+ lab01: +} or {+ main: +}.
	- **action** should be like: {+add+}, {+fix+}, {+implement+} (not {-added-}, {-implemented-}), e.g.
	- No dot at the end of the message.
	- The whole message should give enough info about the commit when using `git log --oneline`

		`raeth: fix regression after [a002b90]`

		`init: fix zone boundary creation`

		`usb, host: avoid devision by zero`

### :bulb: Repository structure

- Do **not** use **Cyrillic** in file or directory names.
- The required items are shown below.

```md
surname_name
├── lab01
│   ├── doc
│   │   └── Reports in odt/doc/docx + pdf formats
│   │       e.g.: lab01.docx, lab01.pdf
│   ├── src
│   │   └── Source files, i.e. ".c" and ".h" files
│   │       e.g.: main.c, lib.c, lib.h
│   ├── README.md 
│   │   contains the task of the lab work.
│   └── ...
├── lab02
│   └── ...
└── ...
```

### :bulb: Use *cppcheck* and compile for debugging

```bash
cppcheck --enable=all --addon=cert.py --xml . 2>report.xml
cppcheck-htmlreport --file=report.xml --report-dir report --source-dir=.

gcc -ansi -pedantic -Wall -Wextra -Og -g source_file.c
```
